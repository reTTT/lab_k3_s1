TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp ../common/vector.h ../common/matrix.h

include(deployment.pri)
qtcAddDeployment()

