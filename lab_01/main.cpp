#include <iostream>
#include <ctime>

using namespace std;

class DataTime
{
public:

    DataTime(time_t seconds) : seconds(seconds) {}

    DataTime()
    {
        time (&seconds);
    }

    static DataTime HourMinSec(int hour, int min, int sec)
    {
        DataTime t;
        time (&t.seconds);
        struct tm * timeinfo;
        timeinfo = localtime (&t.seconds);

        timeinfo->tm_sec = sec;
        timeinfo->tm_min = min;
        timeinfo->tm_hour = hour;

        t.seconds = mktime ( timeinfo );

        return t;
    }

    static DataTime DayMonYear(int day, int mon, int year)
    {
        DataTime t;

        time (&t.seconds);
        struct tm * timeinfo;
        timeinfo = localtime (&t.seconds);

        timeinfo->tm_mday = day;
        timeinfo->tm_mon = mon - 1;
        timeinfo->tm_year = year - 1900;

        t.seconds = mktime ( timeinfo );

        return t;
    }

    static DataTime DayMonYearHourMinSec(int day, int mon, int year, int hour, int min, int sec)
    {
        DataTime t;

        time (&t.seconds);
        struct tm * timeinfo;
        timeinfo = localtime (&t.seconds);

        timeinfo->tm_mday = day;
        timeinfo->tm_mon = mon - 1;
        timeinfo->tm_year = year - 1900;

        timeinfo->tm_sec = sec;
        timeinfo->tm_min = min;
        timeinfo->tm_hour = hour;

        t.seconds = mktime ( timeinfo );

        return t;
    }

    time_t GetSeconds() const
    {
        return seconds;
    }

    DataTime operator +(const DataTime& t) const
    {
        return DataTime(seconds + t.seconds);
    }

    DataTime operator -(const DataTime& t) const
    {
        return DataTime(seconds - t.seconds);
    }

    DataTime& operator +=(const DataTime& t)
    {
        seconds += t.seconds;
        return *this;
    }

    DataTime& operator ++()
    {
        ++seconds;
        return *this;
    }

    DataTime operator ++(int)
    {
        DataTime temp(seconds);
        ++seconds;
        return temp;
    }

    bool operator ==(const DataTime& t) const
    {
        return seconds == t.seconds;
    }

    bool operator !=(const DataTime& t) const
    {
        return seconds != t.seconds;
    }

    bool operator >(const DataTime& t) const
    {
        return seconds > t.seconds;
    }

    bool operator <(const DataTime& t) const
    {
        return seconds < t.seconds;
    }

    bool operator >=(const DataTime& t) const
    {
        return seconds >= t.seconds;
    }

    bool operator <=(const DataTime& t) const
    {
        return seconds <= t.seconds;
    }


    friend std::ostream& operator << (std::ostream& os, const DataTime& t)
    {
        os << ctime(&t.seconds);
        return os;
    }

protected:

    time_t seconds;
};

int main()
{

    {
        DataTime t(60);
        cout << t << endl;
        cout << (t == t) << endl;
        cout << (t != t) << endl;
        cout << (t > t) << endl;
        cout << (t < t) << endl;
        cout << (t >= t) << endl;
        cout << (t <= t) << endl;
    }

    {
        DataTime t;
        cout << t << endl;
    }

    {
        DataTime t = DataTime::HourMinSec(12, 00, 10);
        cout << t << endl;
    }

    {
        DataTime t = DataTime::DayMonYear(14, 5, 1980);
        cout << t << endl;
    }

    {
        DataTime t = DataTime::DayMonYearHourMinSec(14, 5, 1980, 12, 00, 10);
        cout << t << endl;
    }

    cout << "Hello World!" << endl;
    return 0;
}

