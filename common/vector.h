#ifndef __VECTOR_H_
#define __VECTOR_H_

#include <iostream>
#include <cmath>
#include <stdarg.h>

template<class Ty>
class Vector
{
public:

    Vector(size_t size) : size(size)
    {
        data = new Ty[size];

        for(size_t i =0; i<size; ++i)
        {
            data[i] = 0;
        }
    }

    Vector(size_t size, Ty def) : size(size)
    {
        data = new Ty[size];

        for(size_t i =0; i<size; ++i)
        {
            data[i] = def;
        }
    }

    Vector(const Vector& v) : size(v.size)
    {
        if(this == &v)
            return;

        data = new Ty[size];

        for(size_t i=0; i<size; ++i)
        {
            data[i] = v.data[i];
        }
    }

/*
    Vector( std::initializer_list<Ty> list )
    {
        data = new Ty[_s_];
        size = _s_;

        size_t i = 0;

        for( auto e : list )
        {
            data[i++] = e;

            if( i>= size)
                break;
        }
    }
*/

    virtual ~Vector()
    {
        delete[] data;
    }

    static Vector<Ty> create(size_t size, ... )
    {
        Vector<Ty> t(size);

        va_list ap;

        va_start(ap, size);

        for(size_t i=0; i<size; ++i)
        {
            t[i] = va_arg(ap, Ty);
        }

        va_end(ap);

        return t;
    }

    const Ty& operator [](size_t idx) const
    {
        return data[idx];
    }

    Ty& operator [](size_t idx)
    {
        return data[idx];
    }

    friend std::ostream& operator << (std::ostream& s, Vector& v)
    {
        s << "{";
        for(size_t i=0; i<v.size; ++i)
        {
            s << v.data[i];
            if(i < v.size-1)
                s << ", ";
        }

        s << "}";
        return s;
    }

    Vector<Ty> operator * (const Ty& s) const
    {
        Vector<Ty> t(size);

        for(size_t i=0; i<size; ++i)
        {
            t.data[i] = data[i] * s;
        }

        return t;
    }

    Vector<Ty>& operator *= (const Ty& s)
    {
        for(size_t i=0; i<size; ++i)
        {
            data[i] *= s;
        }

        return *this;
    }

    Vector<Ty> operator + (const Vector<Ty>& v) const
    {
        Vector<Ty> t(size);

        for(size_t i=0; i<size; ++i)
        {
            t.data[i] = data[i] + v.data[i];
        }

        return t;
    }

    Vector<Ty>& operator += (const Vector<Ty>& v)
    {
        for(size_t i=0; i<size; ++i)
        {
            data[i] += v.data[i];
        }

        return *this;
    }

    Vector<Ty> operator - (const Vector<Ty>& v) const
    {
        Vector<Ty> t(size);

        for(size_t i=0; i<size; ++i)
        {
            t.data[i] = data[i] - v.data[i];
        }

        return t;
    }

    Vector<Ty>& operator -= (const Vector<Ty>& v)
    {
        for(size_t i=0; i<size; ++i)
        {
            data[i] -= v.data[i];
        }

        return *this;
    }

    Vector<Ty>& operator = (const Vector<Ty>& v)
    {
        if(this == &v)
        {
            return *this;
        }

        delete[] data;

        size = v.size;
        data = new Ty[size];

        for(size_t i=0; i<size; ++i)
        {
            data[i] = v.data[i];
        }

        return *this;
    }

    Ty operator () ()
    {
        Ty t = 0;

        for(size_t i=0; i<size; ++i)
        {
            t += data[i] * data[i];
        }

        return sqrt(t);
    }

    Vector<Ty> operator - () const
    {
        Vector<Ty> t(size);

        for(size_t i=0; i<size; ++i)
        {
            t.data[i] = -data[i];
        }

        return t;
    }

protected:

    Ty* data;
    size_t size;
};

#endif
