#include <iostream>
#include "../common/vector.h"

using namespace std;

int main()
{
    Vector<int> v(Vector<int>::create(3, 3, 2, 1));

    cout << v << std::endl;

    v[0] = 1;

    cout << v << std::endl;

    v *= 2;
    cout << v << std::endl;
    v = -v;
    cout << v << std::endl;
    cout << v() << std::endl;


    cout << "Hello World!" << endl;
    return 0;
}

